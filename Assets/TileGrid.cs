﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using Priority_Queue;
using System.Linq;
using System;
using System.Diagnostics;
using System.Threading;

public class TileGrid : MonoBehaviour {

    private Tile[][] tileArray = new Tile[20][];
    private GameObject[][] tileObjArray = new GameObject[20][]; //visual layout
    public GameObject squareObj; //object for cloning
    private const int dimension = 20;

    private Dictionary<Tile, Tile> from = new Dictionary<Tile, Tile>();
    private Dictionary<Tile, int> steps = new Dictionary<Tile, int>();

    private int blockedTileCount = 100;
    private List<Tile> blockedTileList = new List<Tile>();

    private Tile startPoint;
    private Tile endPoint;

    void Start()
    {
        Init();
        FindPath();
    }

    public void Init()
    {
        CreateTileSet();
        CreateObjGrid();

        CreateBlockedTiles();
        PaintBlockedTiles(Color.gray);

        startPoint = AssignPoint(true);
        endPoint = AssignPoint(false);
    }

    public void FindPath()
    {
        FindPath(startPoint, endPoint);
    }

    private void CreateTileSet()
    {
        for (int i = 0; i < tileArray.Length; i++)
        {
            tileArray[i] = new Tile[dimension];

            for (int j = 0; j < tileArray[i].Length; j++)
            {
                tileArray[i][j] = new Tile();
                tileArray[i][j].SetData(i, j, false, false, false, null);
            }
        }
    }

    private void CreateObjGrid() // Visual Representation
    {
        for (int i = 0; i < tileObjArray.Length; i++)
        {
            tileObjArray[i] = new GameObject[dimension];

            for (int j = 0; j < tileObjArray[i].Length; j++)
            {
                if (tileObjArray[i][j] == null)
                {
                    squareObj.name = i + " " + j;
                    tileObjArray[i][j] = (GameObject)Instantiate(squareObj, this.gameObject.transform);
                    tileObjArray[i][j].SetActive(true);
                    PaintTile(tileObjArray[i][j], Color.white);
                    RectTransform rectTransform = tileObjArray[i][j].GetComponent<RectTransform>();
                    rectTransform.anchoredPosition = new Vector2(i * 36, j * -36);
                }
            }
        }
    }

    private void CreateBlockedTiles()
    {
        blockedTileList.Clear();
        //blockedTileList = null;
        for (int i = 0; i < blockedTileCount; i++)
        {
            blockedTileList.Add(ShutTile());
        }
    }

    private void PaintBlockedTiles(Color value)
    {
        foreach (Tile sq in blockedTileList)
        {
            tileObjArray[sq._x][sq._y].gameObject.GetComponent<Image>().color = value;
        }
    }

    private void PaintTile(GameObject tileObj, Color value)
    {
        tileObj.gameObject.GetComponent<Image>().color = value;
    }

    private Tile ShutTile() //not re-visiting previously blocked tiles
    {
        int x = UnityEngine.Random.Range(0, dimension);
        int y = UnityEngine.Random.Range(0, dimension);
        tileArray[x][y].shut = true;    
        return tileArray[x][y];
    }

    private Tile AssignPoint(bool start)
    {
        int x = UnityEngine.Random.Range(0, dimension);
        int y = UnityEngine.Random.Range(0, dimension);

        if (start)
        {
            tileArray[x][y].startTile = true;
            PaintTile(tileObjArray[x][y], Color.green);
            UnityEngine.Debug.LogFormat("Start {0},{1}", x, y);
            return tileArray[x][y];

        }
        else
        {
            tileArray[x][y].endTile = true;
            PaintTile(tileObjArray[x][y], Color.red);
            UnityEngine.Debug.LogFormat("End {0},{1}", x, y);
            return tileArray[x][y];
        }
    }

    private void FindPath(Tile start, Tile end)
    {
        SimplePriorityQueue<Tile> openQueue = new SimplePriorityQueue<Tile>();
        List<Tile> solution = new List<Tile>();

        openQueue.Enqueue(start, 0);
        steps.Clear();
        from.Clear();
        from[start] = start;

        steps[start] = 0;

        Stopwatch stopWatch = new Stopwatch();
        stopWatch.Start();

        while (openQueue.Count > 0)
        {
            Tile active = openQueue.Dequeue();

            if (active == end)
            {
                while (active.parent != null)
                {
                    solution.Add(active.parent);
                    active = active.parent;

                }
                solution.Reverse();

                foreach (Tile sq in solution)
                {
                    PaintTile(tileObjArray[sq._x][sq._y], Color.yellow);
                    PaintTile(tileObjArray[startPoint._x][startPoint._y], Color.green);
                    PaintTile(tileObjArray[endPoint._x][endPoint._y], Color.red);
                }
                UnityEngine.Debug.LogFormat("Solved in {0} steps: ", solution.Count);
                break;
            }

            bool isEmpty = !solution.Any();
            if (isEmpty)
            {
                foreach (Tile next in CheckNeighbours(active))
                {
                    int newCost = steps[active] + 1;

                    if (!steps.ContainsKey(next) || newCost < steps[next])
                    {
                        if (steps.ContainsKey(next))
                        {
                            steps.Remove(next);
                            from.Remove(next);
                        }
                      
                        steps.Add(next, newCost);
                        from.Add(next, active);
                        int priority = newCost + Heuristic(next, end);
                        openQueue.Enqueue(next, priority);
                        next.parent = active;
                        from[next] = active;
                    }
                }
            }
        }
        stopWatch.Stop();
        TimeSpan ts = stopWatch.Elapsed;

        string elapsedTime = string.Format("{0:00}:{1:00}:{2:00}.{3:00}",
            ts.Hours, ts.Minutes, ts.Seconds,
            ts.Milliseconds / 10);
        UnityEngine.Debug.Log("RunTime " + elapsedTime + " note: path painting is currently included in the while loop, not optimised)");
    }

    private int Heuristic(Tile sqStart, Tile sqEnd)
    {
        var D1 = 1;
        var D2 = 2;
        var dx = Mathf.Abs(sqStart._x - sqEnd._x);
        var dy = Mathf.Abs(sqStart._y - sqEnd._y);
        var outcome = D1 * (dx + dy) + (D2 - 2 * D1) * Mathf.Min(dx, dy);
        UnityEngine.Debug.LogFormat("Current Start {0},{1}, outcome is {2}", sqStart._x, sqStart._y, outcome);

        return outcome;
    }

    private List<Tile> CheckNeighbours(Tile sq)
    {
        UnityEngine.Debug.LogFormat("Checking neighbours of {0},{1}", sq._x, sq._y);
        List<Tile> neighbours = new List<Tile>();

        int maxOffSetRange = 2;
        int minOffsetRange = -1;
        int minXOffsetRange = -1;
        int maxXOffSetRange = 2;

        Tile squareTemp = null;

        if (sq._x > 0)
        {
            minXOffsetRange = -1;
            if (sq._x < dimension - 1)
            {
                maxXOffSetRange = 2;
            }
            else
            {
                maxXOffSetRange = 1;
            }
        }
        else
        {
            minXOffsetRange = 0;
        }

        if (sq._y > 0)
        {
            minOffsetRange = -1;
            if (sq._y < dimension - 1)
            {
                maxOffSetRange = 2;
            }
            else
            {
                maxOffSetRange = 1;
            }
        }
        else
        {
            minOffsetRange = 0;
        }

        for (int i = minXOffsetRange; i < maxXOffSetRange; i++)
        {
            for (int j = minOffsetRange; j < maxOffSetRange; j++)
            {
                if (i == 0 && j == 0)
                {
                    UnityEngine.Debug.Log("Excluding start object");
                }
                else
                {
                    squareTemp = CheckSquare(sq, i, j);

                    if (squareTemp != null)
                    {
                        if (!squareTemp.shut || (squareTemp.shut && squareTemp == endPoint))
                        {
                            UnityEngine.Debug.LogFormat("Added square {0},{1}", squareTemp._x, squareTemp._y);
                            neighbours.Add(squareTemp);
                            PaintTile(tileObjArray[squareTemp._x][squareTemp._y], Color.cyan);
                        }
                    }
                }
            }
        }

        return neighbours;
    }

    private Tile CheckSquare(Tile square, int xOffset, int yOffset)
    {
        if (tileArray[square._x + xOffset][square._y + yOffset] != null)
        {
            return tileArray[square._x + xOffset][square._y + yOffset];
        }
        return null;      
    }
}
