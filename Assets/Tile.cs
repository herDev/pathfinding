﻿
public class Tile
{
    public int _x, _y;
    public bool startTile, endTile;
    public bool shut;
    public Tile parent = null;

    public void SetData(int x, int y, bool _start, bool _end, bool _shut, Tile _parent)
    {
        _x = x;
        _y = y;
        startTile = _start;
        endTile = _end;
        shut = _shut;
        parent = _parent;
    }
}
